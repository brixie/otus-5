//
//  SuffixProcessor.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation
import Collections

protocol CountedSuffixProcessor {
    var suffixes: [CountedSuffix] { get }
    init(sequence: String)
}

func suffixProcessor(_ sequence: String) -> [Suffix] {
    return sequence
        .words
        .map(SuffixSequence.init)
        .flatMap { $0 }
        .map(Suffix.init(content:))
}

struct SuffixProcessor: CountedSuffixProcessor {
    
    private(set) var suffixes: [CountedSuffix] = []
    
    private var counter = Counter<Suffix>()
    
    init(sequence: String) {
        let suffixes = suffixProcessor(sequence)
        
        suffixes.forEach { counter.register($0) }
        
        self.suffixes = Array(OrderedSet(suffixes))
            .map {
                CountedSuffix(suffix: $0, count: counter.count(for: $0))
            }
    }
}
