//
//  TopTriadSuffixProcessor.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

struct TopTriadSuffixProcessor: CountedSuffixProcessor {
    let suffixes: [CountedSuffix]
    private var counter = Counter<Suffix>()
    
    init(sequence: String) {
        self.suffixes = SuffixProcessor(sequence: sequence)
            .suffixes
            .filter(\.content.isTriad)
            .prefix(10)
            .asArray
    }
}
