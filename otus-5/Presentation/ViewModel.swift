//
//  ViewModel.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

class ViewModel: ObservableObject {
    
    enum Selection: CaseIterable {
        case all
        case topTriads
        
        var asString: String {
            switch self {
            case .all: return "All"
            case .topTriads: return "Top Triads"
            }
        }
    }
    
    enum Sorting: CaseIterable {
        case none
        case ascending
        case descending
        
        var asString: String {
            switch self {
            case .none: return "None"
            case .ascending: return "Ascending"
            case .descending: return "Descending"
            }
        }
    }
    
    @Published var selection: Selection = .all
    @Published var sorting: Sorting = .none
    
    @Published var query = "abracadabra"
    @Published var searchQuery = ""
    
    @Published var suffixes: [CountedSuffix] = []
    
    init() {
        $query
            .combineLatest(
                $selection,
                $sorting,
                $searchQuery.debounce(
                    for: 0.5,
                    scheduler: RunLoop.main
                )
            ) { [unowned self] query, selection, sorting, searchQuery in
                let suffixes = process(selection: selection, query: query, sorting: sorting)
                
                guard !searchQuery.isEmpty else { return suffixes }
                return suffixes.filter { $0.content.lowercased().contains(searchQuery.lowercased()) }
            }
            .assign(to: &$suffixes)
    }
    
    func process(selection: Selection, query: String, sorting: Sorting) -> [CountedSuffix] {
        var suffixes: [CountedSuffix]
        switch selection {
        case .all:
            suffixes = SuffixProcessor(sequence: query).suffixes
        case .topTriads:
            suffixes = TopTriadSuffixProcessor(sequence: query).suffixes
        }
        
        guard sorting != .none else { return suffixes }
        return suffixes.sorted(by: {
            return sorting == .ascending ? $0 < $1 : $0 > $1
        })
    }
}
