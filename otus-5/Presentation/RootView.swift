//
//  RootView.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import SwiftUI

struct RootView: View {
    
    @ObservedObject var viewModel = ViewModel()
    
    @State private var showingSheet = false
    
    var body: some View {
        NavigationStack {
            VStack {
                TextField("Enter your text", text: $viewModel.query)
                    .padding()
                    .textFieldStyle(.roundedBorder)
                Button("Process") {
                    showingSheet.toggle()
                }
            }
            .sheet(isPresented: $showingSheet) {
                ContentView(viewModel: viewModel)
                    .environmentObject(viewModel)
            }
        }
    }
}
