//
//  ContentView.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import SwiftUI

struct ContentView: View {
    
    @Environment(\.dismiss) var dismiss
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationStack {
            VStack {
                Picker("Type", selection: $viewModel.selection) {
                    ForEach(ViewModel.Selection.allCases, id: \.self) { type in
                        Text(type.asString).tag(type)
                    }
                }
                .padding(.horizontal)
                .pickerStyle(SegmentedPickerStyle())
                .onChange(of: viewModel.selection) { oldValue, newValue in
                    guard oldValue != newValue else { return }
                    viewModel.selection = newValue
                }
                List {
                    ForEach(viewModel.suffixes) { suffix in
                        VStack(alignment: .trailing) {
                            HStack {
                                Text("\(suffix.content)")
                                Text("\(suffix.count)")
                                    .frame(maxWidth: .infinity, alignment: .trailing)
                            }
                        }
                        .frame(maxWidth: .infinity)
                    }
                }
                .listStyle(.plain)
                .scrollContentBackground(.hidden)
            }
            .navigationTitle("Suffix Array")
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $viewModel.searchQuery)
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Menu {
                        Picker("Sorting options", selection: $viewModel.sorting) {
                            ForEach(ViewModel.Sorting.allCases, id: \.self) { type in
                                Text(type.asString).tag(type)
                            }
                        }
                    } label: {
                        Label("Sort", systemImage: "arrow.up.arrow.down")
                    }
                }
            }
        }
    }
}
