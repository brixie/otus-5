//
//  SuffixIterator.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

struct SuffixIterator: IteratorProtocol {
    
    private let string: String
    private var index = 0
    
    init(_ string: String) {
        self.string = string
    }
    
    mutating func next() -> String? {
        guard index < string.count else { return nil }
        index += 1
        return String(string.suffix(index))
    }
}
