//
//  SuffixSequence.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

struct SuffixSequence: Sequence {
    private let string: String
    
    init(_ string: String) {
        self.string = string
    }
    
    func makeIterator() -> SuffixIterator {
        return SuffixIterator(string)
    }
}
