//
//  Suffix.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

struct Suffix: Hashable, Identifiable {
    let content: String

    var id: String { content }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
