//
//  CountedSuffix.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

@dynamicMemberLookup
struct CountedSuffix: Hashable, Comparable, Identifiable {
    private let suffix: Suffix
    let count: Int
    
    init(suffix: Suffix, count: Int) {
        self.suffix = suffix
        self.count = count
    }

    var id: String { return suffix.id }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(suffix.hashValue)
    }
    
    subscript<T>(dynamicMember keyPath: KeyPath<Suffix, T>) -> T {
        return suffix[keyPath: keyPath]
    }
    
    static func < (lhs: CountedSuffix, rhs: CountedSuffix) -> Bool {
        return lhs.content < rhs.content
    }
}
