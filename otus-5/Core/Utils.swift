//
//  Utils.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

extension Set<Suffix> {
    var asArray: Array<Element> {
        return Array(self)
    }
}

extension Set<CountedSuffix> {
    var asArray: Array<Element> {
        return Array(self)
    }
}

extension Array<CountedSuffix>.SubSequence {
    var asArray: Array<Element> {
        return Array(self)
    }
}

extension String {
    var words: [String] {
        split(whereSeparator: \.isLetter.negation).map(String.init)
    }
    
    var isTriad: Bool {
        return count == 3
    }
}

extension Bool {
    var negation: Bool { !self }
}
