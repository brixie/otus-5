//
//  Counter.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import Foundation

struct Counter<Value: Hashable> {
    private var _counter: [Value: Int] = [:]
    
    mutating func register(_ value: Value) {
        let count = _counter[value, default: 0]
        _counter[value] = count + 1
    }
    
    func count(for value: Value) -> Int {
        return _counter[value, default: 0]
    }
}
