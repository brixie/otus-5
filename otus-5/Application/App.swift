//
//  App.swift
//  otus-5
//
//  Created by Vladimir Gusev on 06.07.2024.
//

import SwiftUI

@main
struct _App: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
